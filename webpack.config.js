const path = require('path');
const webpack = require('webpack');

const config = {
	cache: true,
	entry: {
		app: [
			'babel-polyfill',
			'webpack-hot-middleware/client',
			'./src/frontend/index.jsx',
		]
	},
	output: {
		path: path.join(process.cwd(), 'public/js'),
		publicPath: '/js',
		filename: '[name].js',
	},
	module: {
		rules: [{
			test: /\.jsx?$/,
			use: [{
					loader: 'react-hot-loader'
				},
				{
					loader: 'babel-loader'
				}
			],
			exclude: /node_modules/,
		}, ],
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin()
	]
};

module.exports = config;