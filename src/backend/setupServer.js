import path from 'path';
import feathers from 'feathers';
import feathersMongo from 'feathers-mongodb';
import {
  MongoClient
} from 'mongodb';
import rest from 'feathers-rest';
import socketio from 'feathers-socketio';
import hooks from 'feathers-hooks';
import bodyParser from 'body-parser';

import setupReload from './setupReload';
import services from './services';

const app = feathers();

setupReload(app);

app
  .configure(rest())
  .configure(socketio()) 
  .configure(hooks())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({
    extended: true
  }))
  .use(feathers.static(path.join(process.cwd(), 'public')));

const setupServer = async() => {
  const db = await MongoClient.connect('mongodb://localhost:27017/lappy-customizer');
  app.configure(services(db));
  return app;
}

export default setupServer;