import laptop from './laptop';
import component from './component';

const services = (db) => {
	return function () {
		const app = this;
		app.configure(laptop(db));
		app.configure(component(db));
	}
}

export default services;