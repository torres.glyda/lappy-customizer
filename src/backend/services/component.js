import feathersMongo from 'feathers-mongodb';
import Component from '../../models/Component';

const componentsService = (db) => {
  return function () {
    const app = this;
    app.use('/api/components', feathersMongo({
        Model: db.collection('components')
      }))
      .service('/api/components');
  }
}

export default componentsService;