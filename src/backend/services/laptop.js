import feathersMongo from 'feathers-mongodb';
import Component from '../../models/Component';
import mongodb from 'mongodb';
const ObjectId = mongodb.ObjectId;

const laptopsService = (db) => {
  return function () {
    const app = this;
    app.use('/api/laptops', feathersMongo({
        Model: db.collection('laptops')
      }))
      .service('/api/laptops')
      .hooks({
        after: {
          find: [
            async(hook) => {
              let laptops = hook.result;
              for (let laptop of laptops) {
                const ids = laptop.componentIds.map(componentId => ObjectId(componentId));
                const components = await hook.app.service('/api/components').find({
                  query: {
                    _id: {
                      $in: ids
                    }
                  }
                });
                laptop.components = components.map(component => new Component(component.name, component.category, component.price));
                delete laptop.componentIds;
              }
              return Promise.resolve(hook);
            }
          ]
        },
      });
  }
}

export default laptopsService;