import setupServer from './setupServer';

const startServer = async () => {
  const app = await setupServer();
  app.listen(8000, () => {
    console.log('App is running...');
  });
}

startServer();