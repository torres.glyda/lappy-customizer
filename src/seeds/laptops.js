import service from './service';
import mongodb from 'mongodb';
const ObjectId = mongodb.ObjectId;

const seed = async () => {
  const laptopsPath = '/api/laptops';
  const laptopsService = await service(laptopsPath);
  const laptops = [{
    name: 'Lemur 14"',
    price: 649.00,
    desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
    componentIds: [
      ObjectId("596ba86c6cad6a1aa8277fd5"),
      ObjectId("596ba86c6cad6a1aa8277fd6"),
      ObjectId("596ba86c6cad6a1aa8277fd9"),
      ObjectId("596ba86c6cad6a1aa8277fda"),
      ObjectId("596ba86c6cad6a1aa8277fdb"),
      ObjectId("596ba86c6cad6a1aa8277fdc"),
      ObjectId("596ba86c6cad6a1aa8277fdd"),
      ObjectId("596ba86c6cad6a1aa8277fde"),
      ObjectId("596ba86c6cad6a1aa8277fdf"),
    ],
  },
  {
    name: 'Gazelle 15"',
    price: 719.00,
    desc: 'Gazelle’s definitive lines sports a high quality, sleek finish. Its points and curves draws the attention of those more bold, making the Gazelle the perfect maker or hacker companion.',
    componentIds: [
      ObjectId("596ba86c6cad6a1aa8277fd6"),
      ObjectId("596ba86c6cad6a1aa8277fd7"),
      ObjectId("596ba86c6cad6a1aa8277fd9"),
      ObjectId("596ba86c6cad6a1aa8277fda"),
      ObjectId("596ba86c6cad6a1aa8277fdb"),
      ObjectId("596ba86c6cad6a1aa8277fdc"),
      ObjectId("596ba86c6cad6a1aa8277fdd"),
      ObjectId("596ba86c6cad6a1aa8277fde"),
      ObjectId("596ba86c6cad6a1aa8277fdf"),
    ],
  },
  {
    name: 'Kudu 17"',
    price: 939.00,
    desc: 'Kudu’s high quality finish is sharp and clean, appealing to those who prefer sheer and pure simplicity. Its straight edges and lines keep your machine looking new, and keeps you feeling professional.',
    componentIds: [
      ObjectId("596ba86c6cad6a1aa8277fd7"),
      ObjectId("596ba86c6cad6a1aa8277fd8"),
      ObjectId("596ba86c6cad6a1aa8277fd9"),
      ObjectId("596ba86c6cad6a1aa8277fda"),
      ObjectId("596ba86c6cad6a1aa8277fdb"),
      ObjectId("596ba86c6cad6a1aa8277fdc"),
      ObjectId("596ba86c6cad6a1aa8277fdd"),
      ObjectId("596ba86c6cad6a1aa8277fde"),
      ObjectId("596ba86c6cad6a1aa8277fdf"),
    ],
  },
  ];

  await laptopsService.remove(null);
  return laptopsService.create(laptops);
};

export default seed;