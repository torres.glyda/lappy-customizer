import service from './service';

const seed = async() => {
  const componentsPath = '/api/components';
  const componentsService = await service(componentsPath);

  const components = [{
      name: 'Ubuntu 14.0.21 LTS (64-bit)',
      category: 'os',
      price: 0.00,
    }, {
      name: 'Ubuntu 15.32.2 LTS (64-bit)',
      category: 'os',
      price: 0.00,
    }, {
      name: 'Ubuntu 16.04.2 LTS (64-bit)',
      category: 'os',
      price: 0.00,
    },
    {
      name: 'Ubuntu 17.04 (64-bit)',
      category: 'os',
      price: 800.00,
    },
    {
      name: 'i3-7100u (3 MB Cache - 2 Cores - 4 Threads)',
      category: 'processor',
      price: 0.00,
    },
    {
      name: 'i7-7500U (2.7 up to 3.5 - 4 MB Cache - 2 Cores - 4 Threads)',
      category: 'processor',
      price: 3500.00,
    },
    {
      name: 'Messenger Bag',
      category: 'bag',
      price: 850.00,
    },
    {
      name: 'Laptop Backpack',
      category: 'bag',
      price: 900.00,
    },
    {
      name: 'Extra AC Adapter',
      category: 'accessory',
      price: 450.00,
    },
    {
      name: 'Extra Battery',
      category: 'accessory',
      price: 550.00,
    },
    {
      name: 'Extrernal USB DVD-RW Drive',
      category: 'accessory',
      price: 1300.00,
    },
  ];

  await componentsService.remove(null);
  return componentsService.create(components);
};

export default seed;