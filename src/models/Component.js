import { observable } from 'mobx';

class Component {
  @observable name;
  @observable category;
  @observable price;

  constructor(name, category, price) {
    this.name = name;
    this.category = category;
    this.price = price;
  }
}

export default Component;