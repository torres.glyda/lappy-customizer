import {
  observable,
  action,
  computed
} from 'mobx';

class Laptop {
  @observable selectedComponents = [];

  constructor(doc) {
    Object.assign(this, doc);
    this.multipleSelectionCategories = ['bag', 'accessory'];
  }

  @action addComponent(component) {
    this.selectedComponents.push(component);
  }

  @computed get totalPrice() {
    return this.price + this.selectedComponents.reduce((sum, component) => sum + component.price, 0);
  }

  @action addSelectedComponent(component) {
    this.selectedComponents.push(component);
  }

  @action onComponentSelect(component) {
    const selectedComponentCategory = component.category;
    const selectedComponentName = component.name;
    const selectedComponentByCategoryIndex = this.selectedComponents.findIndex(component => component.category === selectedComponentCategory);
    const selectedComponentByNameIndex = this.selectedComponents.findIndex(component => component.name === selectedComponentName);
    const isMultipleSelectionCategory = this.multipleSelectionCategories.includes(selectedComponentCategory);

    if (selectedComponentByCategoryIndex === -1) {
      this.addSelectedComponent(component);
    } else if (isMultipleSelectionCategory) {
      if (selectedComponentByNameIndex === -1) {
        this.addSelectedComponent(component);
      } else {
        this.selectedComponents.splice(selectedComponentByNameIndex, 1);
      }
    } else {
      this.selectedComponents[selectedComponentByCategoryIndex] = component;
    }
  }
}

export default Laptop;