import {
  observable,
  computed,
  action
} from 'mobx';

class LaptopStore {
  @observable laptops;
  @observable selectedLaptop;

  constructor() {
    this.laptops = [];
  }

  set laptops(laptops) {
    this.laptops = laptops;
  }

  get laptops() {
    return this.laptops;
  }

  @action selectLaptop(selectedLaptop) {
    this.selectedLaptop = selectedLaptop;
  }
}

export default new LaptopStore();