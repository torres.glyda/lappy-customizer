import React from 'react';
import { observer } from 'mobx-react';

const SelectedLaptopDisplay = observer(({ classes, laptopStore }) => (
  <div className={classes.selectedLaptop}>
    <span className={classes.name}>
      {laptopStore.selectedLaptop.name}
    </span>
    <div>
      PHP {laptopStore.selectedLaptop.price}
    </div>
    <div className={classes.desc}>
      {laptopStore.selectedLaptop.desc}
    </div>
  </div>
))

export default SelectedLaptopDisplay;