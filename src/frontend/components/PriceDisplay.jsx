import React from 'react';
import { observer } from 'mobx-react';

const PriceDisplay = observer(({ classes, laptopStore }) =>
  <div className={classes.totalPrice}>PHP {laptopStore.selectedLaptop.totalPrice}</div>
);

export default PriceDisplay;