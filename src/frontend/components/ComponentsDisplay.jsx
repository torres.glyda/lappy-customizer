import React from 'react';
import { observer } from 'mobx-react';

const ComponentList = ({ classes, components, type, category, onComponentSelect }) => (
  <div>
    {components.map(component =>
      component.category === category
        ? <label className={classes.label} key={`${category}-${component.name}`}>
          <input className={classes.input} type={type} name={category} onClick={onComponentSelect(component)} />
          <span className={classes.labelSpan}>{component.name}</span> -
          <span> PHP {component.price}</span>
          <br />
        </label>
        : ''
    )}
  </div>
);

const ComponentsDisplay = observer(({ classes, laptopStore, onComponentSelect }) => {
  const components = laptopStore.selectedLaptop.components;
  return <div>
    <h2 className={classes.componentLabel}>Operating System</h2>
    <ComponentList classes={classes} type='radio' category='os' components={components} onComponentSelect={onComponentSelect} />
    <h2 className={classes.componentLabel}>Processors</h2>
    <ComponentList classes={classes} type='radio' category='processor' components={components} onComponentSelect={onComponentSelect} />
    <h2 className={classes.componentLabel}>Bags</h2>
    <ComponentList classes={classes} type='checkbox' category='bag' components={components} onComponentSelect={onComponentSelect} />
    <h2 className={classes.componentLabel}>Accessories</h2>
    <ComponentList classes={classes} type='checkbox' category='accessory' components={components} onComponentSelect={onComponentSelect} />
  </div>
});

export default ComponentsDisplay;