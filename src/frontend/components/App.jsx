import React from 'react';
import { observer, inject } from 'mobx-react';

import LaptopSelection from './LaptopSelection';
import SelectedLaptopDisplay from './SelectedLaptopDisplay';
import ComponentsDisplay from './ComponentsDisplay';
import PriceDisplay from './PriceDisplay';

@inject('laptopStore') @observer
class App extends React.Component {
  laptopStore = this.props.laptopStore;

  onLaptopSelect(laptop) {
    return () => {
      this.laptopStore.selectLaptop(laptop);
    }
  }

  onComponentSelect(component) {
    return () => {
      this.laptopStore.selectedLaptop.onComponentSelect(component);
    }
  }

  render() {
    return <div>
      <div className={this.props.classes.app}>
        <header className={this.props.classes.header}>
          <h2>Lappy Customizer</h2>
        </header>
        <LaptopSelection
          classes={this.props.classes}
          laptopStore={this.laptopStore}
          onLaptopSelect={(laptop) => this.onLaptopSelect(laptop)}
        />
        {
          this.laptopStore.selectedLaptop
            ? <div>
              <PriceDisplay laptopStore={this.laptopStore} classes={this.props.classes} />
              <SelectedLaptopDisplay laptopStore={this.laptopStore} classes={this.props.classes} />
              <ComponentsDisplay laptopStore={this.laptopStore} onComponentSelect={(component) => this.onComponentSelect(component)} classes={this.props.classes} />
            </div>
            : ''
        }
      </div>
    </div>
  }
}

export default (App);