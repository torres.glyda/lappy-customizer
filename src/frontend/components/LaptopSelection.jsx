import React from 'react';
import { observer } from 'mobx-react';

const displayLaptop = (classes, onLaptopSelect) => (laptop, index) => (
  <div key={laptop._id} className={classes.laptop} tabIndex={index} onClick={onLaptopSelect(laptop)}>
    {laptop.name}
  </div>
);

const LaptopSelection = observer(({ classes, laptopStore, onLaptopSelect }) => (
  <div>
    {laptopStore.laptops.map(displayLaptop(classes, onLaptopSelect))}
  </div>
));

export default LaptopSelection;