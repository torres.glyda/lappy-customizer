const styles = {
  app: {
    fontFamily: 'Arial',
    textAlign: 'center',
  },
  header: {
    background: 'salmon',
    lineHeight: '50px',
    marginTop: '-20px',
    marginBottom: '20px',
    color: 'aliceblue',
  },
  laptop: {
    marginTop: '15px',
    lineHeight: '40px',
    cursor: 'pointer',
    background: 'lightGray',
    '&:hover': {
      background: 'tomato',
      color: 'white',
    },
    textAlign: 'center',
  },
  desc: {
    marginTop: '10px',
  },
  totalPrice: {
    marginTop: '25px',
    lineHeight: '40px',
    background: 'dimgray',
    color: 'white',
    fontSize: '20px',
    textAlign: 'center',
  },
  selectedLaptop: {
    marginTop: '10px',
    border: 'gray solid 1px',
    borderRadius: '3px',
    textAlign: 'center',
  },
  name: {
    fontWeight: 'bold',
    fontSize: '25px',
  },
  componentLabel: {
    fontSize: '20px',
    textTransform: 'uppercase',
  },
  input: {
    transform: 'scale(2)',
    marginTop: '20px',
  },
  label: {
    fontSize: '15px',
    height: '90px',
    cursor: 'pointer',
  },
  labelSpan: {
    marginLeft: '20px',
  },
}

export default styles;