import feathers from 'feathers/client';
import feathersSocketio from 'feathers-socketio';
import socketioClient from 'socket.io-client';
import hooks from 'feathers-hooks';

const socketConnection = socketioClient('http://localhost:8000');
const client = feathers();

client
  .configure(feathersSocketio(socketConnection))
  .configure(hooks());

window.app = client;

export default client;