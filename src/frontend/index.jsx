import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import client from './client';
import injectSheet from 'react-jss';
import styles from './styles/styles';

import App from './components/App';
import laptopStore from './stores/laptopStore';
import Laptop from '../models/Laptop';

const storeLaptops = async () => {
  const laptops = await client.service('api/laptops').find();
  laptopStore.laptops = laptops.map(laptop => new Laptop(laptop));
}

storeLaptops();

const StyledApp = injectSheet(styles)(App);

ReactDOM.render(
  <Provider laptopStore={laptopStore}>
    <StyledApp />
  </Provider>,
  document.getElementById('app')
);
